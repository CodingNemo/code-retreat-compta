module TradesTests

open System

open Xunit
open FsUnit.Xunit
open FsUnit.CustomMatchers

open Lib
open Commands
open Events

[<Fact>]
let ``Should create a TradeCreated event`` () =
    let now = DateTime.Now
    let createTradeCommand = {
        LegalEntity = "SG"
        Counterparty = "ANEO"
        Date = now
        Direction = "Buy"
    }

    let evt = createTradeCommand 
              |> Trades.create  

    evt
    |> should ofCase<@ TradeCreated @> 

    match evt with
    | TradeCreated tc ->
        tc.LegalEntity |> should equal "SG"
        tc.Counterparty |> should equal "ANEO"
        tc.Date |> should equal now
        tc.Direction |> should equal Buy