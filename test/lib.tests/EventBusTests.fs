module EventBusTests

open Xunit
open FsUnit.Xunit

open System

open Lib.Commands
open Lib.Events
open Lib.TradeService
open Lib.EventBus

[<Fact>]
let ``Should store a TradeCreated event`` () =
    let commandParams = {
        LegalEntity = "SG"
        Counterparty = "ANEO"
        Date = DateTime.Now
        Direction = "Buy"
    }
   
    let mutable eventStore:Event list = []
    let comm = CreateTrade commandParams
    let store ev = 
        eventStore <- append eventStore ev

    accept comm store 
    |> should equal Success
    eventStore |> should haveLength 1
    