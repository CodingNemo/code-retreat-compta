﻿namespace WebApi.Controllers

open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging

open Lib.Commands
open Lib.Events
open Lib.EventBus
open Lib.TradeService

[<ApiController>]
[<Route("[controller]")>]
type TradeController (logger : ILogger<TradeController>) =
    inherit ControllerBase()

    let mutable eventStore:Event list = []

    let store ev = 
        eventStore <- append eventStore ev

    [<HttpGet>]
    member _.Get() =
        "Hello World!"

    [<HttpGet>]
    [<Route("GetByLegalEntity/{0}")>]
    member _.GetByLegalEntity(legalEntity:string) =
        ReadProjectionByLegalEntity eventStore legalEntity

    [<HttpPost>]
    member this.Post(createTradeCommand:CreateTradeCommand) =
       let command = CreateTrade createTradeCommand
       let result = accept command store
       match result with
       | Success -> this.StatusCode(202)
       | _ -> this.StatusCode(500)
