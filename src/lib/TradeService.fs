namespace lib
open Events
open Trades

module TradeService =
    type ServiceResult =
    | Success
    | Error 

    let accept command store =
        match command with
        | CreateTrade ct -> 
            let ev = create ct
            store ev
            Success

