namespace Lib

open System

module Commands =

    type CreateTradeCommand = {
        LegalEntity: string
        Counterparty: string
        Date: DateTime
        Direction : string
    }

    type Command =
    | CreateTrade of CreateTradeCommand