namespace Lib

open Commands
open Events
open Trades

module EventBus =
    let append (list:Event list) (ev:Event) =
        ev::list
    let ReadProjectionByLegalEntity (list:Event list) legalEntity =
        seq {
            for ev in list do
            match ev with
            | TradeCreated tc -> 
                if tc.LegalEntity = legalEntity then
                    ev
        }


module TradeService =
    type ServiceResult =
    | Success
    | Error 

    let accept command store =
        match command with
        | CreateTrade ct -> 
            let ev = create ct
            store ev
            Success

