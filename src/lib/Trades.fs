﻿namespace Lib

open Commands
open Events

module Trades =
    let create (createCommand:CreateTradeCommand) =
        let tradeParams = {
            Id = TradeId.create() 
            LegalEntity = createCommand.LegalEntity
            Counterparty = createCommand.Counterparty
            Date = createCommand.Date
            Direction = 
                match createCommand.Direction with
                | "Buy" -> Buy
                | "Sell" -> Sell
                | _ -> Sell
        }

        TradeCreated tradeParams
