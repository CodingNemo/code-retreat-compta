namespace Lib

open System

module Events =
    type TradeDirection = 
    | Buy
    | Sell

    type TradeId = string

    module TradeId = 
        let create () : TradeId = 
            Guid.NewGuid().ToString()

    type TradeCreatedEvent = {
        Id: TradeId
        LegalEntity: string
        Counterparty: string
        Date: DateTime
        Direction : TradeDirection
    }

    type Event =
    | TradeCreated of TradeCreatedEvent